package com.gdg.eliseu.wifisignal;

import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.libraries.cast.companionlibrary.cast.BaseCastManager;
import com.google.android.libraries.cast.companionlibrary.cast.DataCastManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WifiSignalActivity extends ActionBarActivity {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    private TextView wifiTextID;
    private Handler handler = new Handler();

    private static final String TAG = WifiSignalActivity.class.getSimpleName();
    private DataCastManager mDataCastManager;

    private WiFiScanner mWifiScanner;
    private List <WiFiInfo> wifiList;
    private ArrayList<String> wifiID;
    private WiFiInfo wifiInfo;
    private int position=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_signal);
        wifiTextID = (TextView) findViewById(R.id.text_wifi);
        listView = (ListView) findViewById(R.id.listView);

        BaseCastManager.checkGooglePlayServices(this);
        mDataCastManager = SenderApplication.getDataCastManager(this);
        mDataCastManager.reconnectSessionIfPossible(/*seconds*/);

        mWifiScanner = new WiFiScanner((WifiManager) this.getSystemService(WIFI_SERVICE));

        wifiList = mWifiScanner.getWifiInfo();
        wifiInfo = wifiList.get(position);

        wifiTextID.setText(wifiInfo.getWifiID());
        wifiID = mWifiScanner.getWifiID();

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, wifiID);
        listView.setAdapter(adapter);

        Worker wk = new Worker();
        wk.start();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "Position :"+position+" ListItem : " +listView.getItemAtPosition(position) , Toast.LENGTH_LONG) .show();

                JSONObject selected = new JSONObject();

                try {
                    selected.put("item", "1");
                    selected.put("selected", listView.getItemAtPosition(position));
                    if(mDataCastManager.isConnected())
                        mDataCastManager.sendDataMessage(selected.toString(), SenderApplication.namespace);
                } catch (Exception e) {
                    Log.i("Debug1", e.getMessage());
                }

                wifiTextID.setText(listView.getItemAtPosition(position).toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_wifi_signal, menu);
        mDataCastManager.addMediaRouterButton(menu, R.id.media_route_menu_item);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDataCastManager = SenderApplication.getDataCastManager(this);
    }

    class Worker extends Thread{
        public void run(){
            while(true){
                try {
                    handler.post(new Runnable() {
                        public void run() {
                            adapter.clear();
                            mWifiScanner.networkScanner();
                            wifiList = mWifiScanner.getWifiInfo();
                            wifiID = mWifiScanner.getWifiID();
                            //Log.i("Debug1", "getWifiID 1! " + wifiID);
                            adapter.notifyDataSetChanged();
                            listView.invalidateViews();
                            listView.refreshDrawableState();
                        }
                    });

                    String json = getDataJson();
                    try {
                        if(mDataCastManager.isConnected())
                            mDataCastManager.sendDataMessage(json, SenderApplication.namespace);
                    } catch (Exception e) {
                        Log.i("Debug1", e.getMessage());
                        Log.i("Debug1", "************* Not connected "+json);
                    }
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getDataJson(){
        JSONObject json = new JSONObject();
        JSONArray[] array;

        synchronized (this){
            try {
                array = new JSONArray[wifiList.size()];

                for (int i=0; i < wifiList.size(); i++){
                    array[i] = new JSONArray();
                    array[i].put(wifiList.get(i).getWifiID());
                    array[i].put(wifiList.get(i).getSecurity());
                    array[i].put(wifiList.get(i).getFrequency());
                    array[i].put(wifiList.get(i).getLevel());
                }

                for (int i=0; i < array.length; i++) {
                    json.put(Integer.toString(i), array[i].toString());
                }

                json.put("item", "0");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return json.toString();
    }
}
