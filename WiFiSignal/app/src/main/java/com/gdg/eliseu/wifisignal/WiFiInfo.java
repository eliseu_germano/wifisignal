package com.gdg.eliseu.wifisignal;

public class WiFiInfo {
    private String wifiID;
    private String security;
    private int frequency;
    private int level;

    public WiFiInfo(String wifiID, String security, int frequency, int level) {
        this.wifiID = wifiID;
        this.security = security;
        this.frequency = frequency;
        this.level = level;
    }

    public String getWifiID() {
        return wifiID;
    }

    public String getSecurity() {
        return security;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getLevel() {
        return level;
    }
}
