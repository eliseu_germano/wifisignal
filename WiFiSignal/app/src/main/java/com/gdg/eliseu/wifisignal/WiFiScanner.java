package com.gdg.eliseu.wifisignal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.gdg.eliseu.wifisignal.WiFiInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class WiFiScanner {

    private WifiManager wifiManager;
    private BroadcastReceiver broadcastReceiver;

    private List <WiFiInfo> wifiList = new LinkedList<>();
    private ArrayList<String> arrayListWifiID = new ArrayList<>();

    public WiFiScanner(WifiManager wifiManager){
        this.wifiManager = wifiManager;

        if(broadcastReceiver == null) broadcastReceiver = new WiFiScanReceiver(this);

        networkScanner();
    }

    public void networkScanner(){
        List<ScanResult> accessPoints = wifiManager.getScanResults();
        if(wifiList.size()>0) wifiList.clear();
        if(arrayListWifiID.size()>0) arrayListWifiID.clear();

        wifiManager.startScan();

        for(int i = 0 ; i < accessPoints.size() ; i++){
            ScanResult ap = accessPoints.get(i);
            wifiList.add(new WiFiInfo(ap.SSID, ap.capabilities, ap.frequency, ap.level));
            arrayListWifiID.add(ap.SSID);
        }

        wifiManager.startScan();
    }

    public List<WiFiInfo> getWifiInfo() {
        return wifiList;
    }

    public ArrayList<String> getWifiID() {
        return arrayListWifiID;
    }

    class WiFiScanReceiver extends BroadcastReceiver {
        WiFiScanner wifiScanner;

        public WiFiScanReceiver(WiFiScanner wifiScanner) {
            super();
            this.wifiScanner = wifiScanner;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            wifiScanner.networkScanner();
        }
    }
}
