package com.gdg.eliseu.wifisignal;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.common.api.Status;

import com.google.android.libraries.cast.companionlibrary.cast.DataCastManager;
import com.google.android.libraries.cast.companionlibrary.cast.callbacks.DataCastConsumerImpl;

public class SenderApplication extends Application {

    private static DataCastManager mDataCastManager;
    private static String app_id = "E8367FFC";
    public static final String namespace = "urn:x-cast:com.gdg.eliseu.wifisignal";

    public static DataCastManager getDataCastManager(Context ctx){
        if(mDataCastManager == null){
            mDataCastManager = DataCastManager.initialize(ctx, app_id, namespace);
            mDataCastManager.enableFeatures(DataCastManager.FEATURE_DEBUGGING);

            mDataCastManager.addDataCastConsumer(new DataCastConsumerImpl() {
                @Override
                public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
                    Log.e("Casting", namespace + "=" + message);
                }

                @Override
                public void onMessageSendFailed(Status status) {
                    Log.e("Casting", "Send failed with status = " + status);
                }
            });
        }
        return mDataCastManager;
    }
}
