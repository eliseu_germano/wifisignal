$(document).ready(function() {
    var width;
    var dadosAntigos = {};
    var dadosAtuais = {};
    var lastSelected = "null";

    cast.receiver.logger.setLevelValue(0);
    window.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();
    
    castReceiverManager.onReady = function(event) {
        console.log('Received Ready event: ' + JSON.stringify(event.data));
        window.castReceiverManager.setApplicationState("Application status is ready...");
    };

    castReceiverManager.onSenderConnected = function(event) {
        console.log('Received Sender Connected event: ' + event.data);
        console.log(window.castReceiverManager.getSender(event.data).userAgent);
    };

    castReceiverManager.onSenderDisconnected = function(event) {
        console.log('Received Sender Disconnected event: ' + event.data);
        if (window.castReceiverManager.getSenders().length == 0) {
            window.close();
        }
    };

    castReceiverManager.onSystemVolumeChanged = function(event) {
        console.log('Received System Volume Changed event: ' + event.data['level'] + ' ' +
        event.data['muted']);
    };

    window.messageBus = window.castReceiverManager.getCastMessageBus('urn:x-cast:com.gdg.eliseu.wifisignal');

    window.messageBus.onMessage = function(event) {
        
        
        console.log(event.data);
        var dados = $.parseJSON(event.data);

        if(dados['item']=='0'){
            delete dados.item;

            dadosAtuais = displayText(dados);
            dadosAntigos = compare(dadosAntigos, dadosAtuais);
            window.messageBus.send(event.senderId, event.data);
        } else if (dados['item']=='1'){
            $("#progress-"+dados['selected']).css("background-color", "#337ab7");
            
            if(lastSelected != "null"){
                $("#progress-"+lastSelected).css("background-color", "#d9534f");
                $("#id").remove();
                $("#security").remove();
                $("#frequency").remove();
                $("#level").remove();
            }
            
            lastSelected = dados['selected'];
            $('.wifi-info').append('<li id="id">Wi-Fi ID: <br>'+lastSelected+' <br></li>');
            $('.wifi-info').append('<li id="security">Security: <br> '+dadosAntigos[lastSelected][0]+' <br></li>');
            $('.wifi-info').append('<li id="frequency">Frequency: <br> '+dadosAntigos[lastSelected][1]+' <br></li>');
            $('.wifi-info').append('<li id="level">Level: <br> '+dadosAntigos[lastSelected][2]+' DB </li>');
        }
    }
    
    window.castReceiverManager.start({statusText: "Application is starting"});
    console.log('Receiver Manager started');
});

function compare(dadosAntigos, dadosAtuais){
    var numItens = Object.keys(dadosAntigos).length;

    if($.isEmptyObject(dadosAntigos)) {
        $.each(dadosAtuais, function(key, value){
            newLi = '<li id="id-'+key+'"><div class="key pull-right"> DB </div> <div class="pontencia"> <div class="info">';
            newLi += key; // ID_Wifi
            newLi += '</div> <div class="progress progress-small"> <div style="width: ';
            newLi += (100+parseInt(value[2]));
            newLi += '%;" class="progress-bar progress-bar-danger" id="progress-'+key+'"></div></div></div></li>';
            
            if(numItens < 8){
                $('.wifi-list').append(newLi);
                dadosAntigos[key] = value;
                numItens++;
            }
        });
    } else {
         $.each(dadosAtuais, function(key, value){
            if(!(key in dadosAntigos)){
                newLi = '<li id="id-'+key+'"><div class="key pull-right"> DB </div> <div class="pontencia"> <div class="info">';
                newLi += key; // ID_Wifi
                newLi += '</div> <div class="progress progress-small"> <div style="width: ';
                newLi += (100+parseInt(value[2]));
                newLi += '%;" class="progress-bar progress-bar-danger" id="progress-'+key+'"></div></div></div></li>';
                
                if(numItens < 8){
                    $('.wifi-list').append(newLi);
                    dadosAntigos[key] = value;
                    numItens++;
                }

            } else if(dadosAntigos[key] !== value) {
                $('#progress-'+key).css('width', (100+parseInt(value[2]))+"%");
                dadosAntigos[key] = value;
            }
        });

        console.log("Tamanho DAn = "+ Object.keys(dadosAntigos).length + " \nTamanho DAt = "+ Object.keys(dadosAtuais).length);

        if(Object.keys(dadosAntigos).length > Object.keys(dadosAtuais).length){
            
            $.each(dadosAntigos, function(key, value){
                if(!(key in dadosAtuais)){
                    $("#id-"+key).remove();
                    delete dadosAntigos[key];
                    numItens--;
                }
            });
        }
    }
    return dadosAntigos;
}

// utility function to display the text message in the input field
function displayText(dados) {
    dadosAtuais = {};

    $.each(dados, function(key, value){
        array = $.parseJSON(value);
        
        if(array[0].length !== 0)
            dadosAtuais[array[0]] = {0 : array[1], 1 : array[2], 2 : array[3]}
    });
    
    window.castReceiverManager.setApplicationState(dados);
    console.log(dadosAtuais);
    return dadosAtuais;
}

function setInformation(){

}