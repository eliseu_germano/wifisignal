# README #

Simple Chromecast App for using the Google Cast Api with Cast Companion Library Android and a custom receiver. Built to lecture at GDG DevFest 2015.

### Configuration ###
 Edit SenderApplication.java to your Cast App_ID and namespace (WiFiSignal)

 Edit receiver.js to your Cast App_ID and namespace (WiFiSignal_Receiver)

### Dependencies ###
 Cast Campanion Library